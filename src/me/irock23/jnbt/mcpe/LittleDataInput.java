/**
 * Copyright 2014 David Vick
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.irock23.jnbt.mcpe;

import java.io.DataInput;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class LittleDataInput implements DataInput {
	private final DataInput in;
	private final ByteBuffer buffer = ByteBuffer.allocate(8);
	
	public LittleDataInput(DataInput in) {
		this.in = in;
	}
	
	public DataInput getDataInput() {
		return in;
	}

	@Override
	public boolean readBoolean() throws IOException {
		return in.readBoolean();
	}

	@Override
	public byte readByte() throws IOException {
		return in.readByte();
	}

	@Override
	public char readChar() throws IOException {
		buffer.clear();
		buffer.order(ByteOrder.LITTLE_ENDIAN).putChar(in.readChar()).flip();
		return buffer.order(ByteOrder.BIG_ENDIAN).getChar();
	}

	@Override
	public double readDouble() throws IOException {
		buffer.clear();
		buffer.order(ByteOrder.LITTLE_ENDIAN).putDouble(in.readDouble()).flip();
		return buffer.order(ByteOrder.BIG_ENDIAN).getDouble();
	}

	@Override
	public float readFloat() throws IOException {
		buffer.clear();
		buffer.order(ByteOrder.LITTLE_ENDIAN).putFloat(in.readFloat()).flip();
		return buffer.order(ByteOrder.BIG_ENDIAN).getFloat();
	}

	@Override
	public void readFully(byte[] b) throws IOException {
		in.readFully(b);
	}

	@Override
	public void readFully(byte[] b, int off, int len) throws IOException {
		in.readFully(b, off, len);
	}

	@Override
	public int readInt() throws IOException {
		buffer.clear();
		buffer.order(ByteOrder.LITTLE_ENDIAN).putInt(in.readInt()).flip();
		return buffer.order(ByteOrder.BIG_ENDIAN).getInt();
	}

	@Override
	public String readLine() throws IOException {
		return in.readLine();
	}

	@Override
	public long readLong() throws IOException {
		buffer.clear();
		buffer.order(ByteOrder.LITTLE_ENDIAN).putLong(in.readLong()).flip();
		return buffer.order(ByteOrder.BIG_ENDIAN).getLong();
	}

	@Override
	public short readShort() throws IOException {
		buffer.clear();
		buffer.order(ByteOrder.LITTLE_ENDIAN).putShort(in.readShort()).flip();
		return buffer.order(ByteOrder.BIG_ENDIAN).getShort();
	}

	@Override
	public String readUTF() throws IOException {
		return in.readUTF();
	}

	@Override
	public int readUnsignedByte() throws IOException {
		return in.readUnsignedByte();
	}

	@Override
	public int readUnsignedShort() throws IOException {
		return readShort() & 0xFFFF;
	}

	@Override
	public int skipBytes(int n) throws IOException {
		return in.skipBytes(n);
	}

}
