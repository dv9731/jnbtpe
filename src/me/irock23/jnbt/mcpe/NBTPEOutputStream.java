/**
 * Copyright 2014 David Vick
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.irock23.jnbt.mcpe;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteOrder;

import me.irock23.jnbt.NBTOutputStream;

public class NBTPEOutputStream extends NBTOutputStream {
	public NBTPEOutputStream(OutputStream out, byte[] header) throws IOException {
		this(out);
		this.out.write(header);
	}
	
	public NBTPEOutputStream(OutputStream out, int type, int length) throws IOException { //for level.dat
		this(out);
		this.out.writeInt(type);
		this.out.writeInt(length);
	}
	
	private NBTPEOutputStream(OutputStream out) {
		super(new LittleDataOutput(new DataOutputStream(out)), false); //LittleDataOutput only actually necessary for type & length
		stream = (DataOutputStream) ((LittleDataOutput) this.out).getDataOutput();
		byteOrder = ByteOrder.LITTLE_ENDIAN;
	}
}
