/**
 * Copyright 2014 David Vick
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.irock23.jnbt.mcpe;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import me.irock23.jnbt.NBTInputStream;

public class NBTPEInputStream extends NBTInputStream {
	private final byte[] header;

	public NBTPEInputStream(InputStream in, int headerSize) throws IOException {
		super(new LittleDataInput(new DataInputStream(in)), false);
		stream = (DataInputStream) ((LittleDataInput) this.in).getDataInput();
		byteOrder = ByteOrder.LITTLE_ENDIAN;
		header = new byte[headerSize];
		this.in.readFully(header);
	}
	
	public NBTPEInputStream(InputStream in) throws IOException { //default to level.dat headerSize
		this(in, 8);
	}
	
	public byte[] getHeader() {
		return header;
	}
	
	public ByteBuffer getHeaderBuffer() {
		return ByteBuffer.wrap(header).order(byteOrder);
	}
}
