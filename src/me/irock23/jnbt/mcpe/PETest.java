package me.irock23.jnbt.mcpe;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;

import me.irock23.jnbt.NBT;

public class PETest {
	public static void main(String[] args) throws Exception {
		//Tag.setDefaultByteOrder(ByteOrder.LITTLE_ENDIAN); //should work without setting this (more efficient if set, though)
		NBTPEInputStream in = new NBTPEInputStream(new FileInputStream(new File("level.dat")));
		NBT peLevel = in.readNBT();
		in.close();
		ByteBuffer header = in.getHeaderBuffer();
		System.out.println(header.getInt() + " - " + header.getInt());
		System.out.println(peLevel.print());
		if (args.length > 0 && args[0] == "writetest") {
			header.flip();
			NBTPEOutputStream out = new NBTPEOutputStream(new FileOutputStream(new File("level-out.dat")), header.getInt(), header.getInt());
			out.writeNBT(peLevel);
			out.close();
			System.out.println("-------------------------------------");
			in = new NBTPEInputStream(new FileInputStream(new File("level-out.dat")));
			NBT peLevelOut = in.readNBT();
			in.close();
			header = in.getHeaderBuffer();
			System.out.println(header.getInt() + " - " + header.getInt());
			System.out.println(peLevelOut.print());
		}
	}
}
