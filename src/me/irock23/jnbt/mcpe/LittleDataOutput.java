/**
 * Copyright 2014 David Vick
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.irock23.jnbt.mcpe;

import java.io.DataOutput;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class LittleDataOutput implements DataOutput {
	private final DataOutput out;
	private final ByteBuffer buffer = ByteBuffer.allocate(8);
	
	public LittleDataOutput(DataOutput out) {
		this.out = out;
	}
	
	public DataOutput getDataOutput() {
		return out;
	}

	@Override
	public void write(int b) throws IOException {
		out.write(b);
	}

	@Override
	public void write(byte[] b) throws IOException {
		out.write(b);
	}

	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		out.write(b, off, len);
	}

	@Override
	public void writeBoolean(boolean b) throws IOException {
		out.writeBoolean(b);
	}

	@Override
	public void writeByte(int b) throws IOException {
		out.writeByte(b);
	}

	@Override
	public void writeBytes(String s) throws IOException {
		out.writeBytes(s);
	}

	@Override
	public void writeChar(int c) throws IOException {
		writeShort(c);
	}

	@Override
	public void writeChars(String s) throws IOException {
		for (int i = 0; i < s.length(); i++) writeChar(s.charAt(i));
	}

	@Override
	public void writeDouble(double d) throws IOException {
		buffer.clear();
		buffer.order(ByteOrder.BIG_ENDIAN).putDouble(d).flip();
		out.writeDouble(buffer.order(ByteOrder.LITTLE_ENDIAN).getDouble());
	}

	@Override
	public void writeFloat(float f) throws IOException {
		buffer.clear();
		buffer.order(ByteOrder.BIG_ENDIAN).putFloat(f).flip();
		out.writeFloat(buffer.order(ByteOrder.LITTLE_ENDIAN).getFloat());
	}

	@Override
	public void writeInt(int i) throws IOException {
		buffer.clear();
		buffer.order(ByteOrder.BIG_ENDIAN).putInt(i).flip();
		out.writeInt(buffer.order(ByteOrder.LITTLE_ENDIAN).getInt());
	}

	@Override
	public void writeLong(long l) throws IOException {
		buffer.clear();
		buffer.order(ByteOrder.BIG_ENDIAN).putLong(l).flip();
		out.writeLong(buffer.order(ByteOrder.LITTLE_ENDIAN).getLong());
	}

	@Override
	public void writeShort(int s) throws IOException {
		buffer.clear();
		buffer.order(ByteOrder.BIG_ENDIAN).putShort((short) s).flip();
		out.writeShort(buffer.order(ByteOrder.LITTLE_ENDIAN).getShort() & 0xFFFF);
	}

	@Override
	public void writeUTF(String s) throws IOException {
		out.writeUTF(s);
	}

}
